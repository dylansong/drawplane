import Game from './Game';
const game = new Game(2048,1536);
import Boot from './states/Boot'
game.state.add('Boot',Boot);
import Preload from './states/Preload'
game.state.add('Preload',Preload);
import Menu1 from './states/Menu1'
game.state.add('Menu1',Menu1);
import Menu2 from './states/Menu2'
game.state.add('Menu2',Menu2);
import Menu3 from './states/Menu3'
game.state.add('Menu3',Menu3);
import Menu4 from './states/Menu4'
game.state.add('Menu4',Menu4);
import Menu5 from './states/Menu5'
game.state.add('Menu5',Menu5);
import PlayGame from './states/PlayGame'
game.state.add('PlayGame',PlayGame);
import GameOver from './states/GameOver'
game.state.add('GameOver',GameOver);

game.state.start('Boot');