export default class Preload extends Phaser.State{
    preload(){



    }
    create(){

        this.add.sprite(45.5,76,'top');
        this.dinosaurBar = this.add.sprite(1441,96,'dinosaurBar');
        this.childrenBar = this.add.sprite(813,95,'childrenBar');
        this.timeBar = this.add.sprite(284,95.5,'timeBar');
        this.children = this.add.sprite(82.5,1275,'children');
        this.dinosaur = this.add.sprite(1017,507,'dinosaur');
        this.missileButton = this.add.sprite(1343,187.5,'missile');
        this.missileButton2 = this.add.sprite(1538,187.5,'missile');
        this.missileButton3 = this.add.sprite(1739,187.5,'missile');


        this.continueBtn = this.add.button(this.world.width-200,this.world.centerY*1.525,'continue-btn',this.enterGameOver);
        this.continueBtn.anchor.setTo(.5);


        
   
        


    }
    enterGameOver(){
        this.game.state.start('GameOver');
    }
}