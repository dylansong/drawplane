export default class Preload extends Phaser.State{
    preload(){
        // this.load.image('m1-game-rule','./assets/sprites/imgout/1/1-game-rule.png');
        // this.load.image('m1-start-btn','./assets/sprites/imgout/1/1-start-btn.png');
        // this.load.image('m1-title','./assets/sprites/imgout/1/1-title.png');
        // this.load.image('m2-title','./assets/sprites/imgout/2/2-title.png');
        // this.load.image('continue-btn','./assets/sprites/imgout/2/continue-btn.png');
        // this.load.image('draw-board','./assets/sprites/imgout/2/drawboard.png');
        // this.load.image('m3-title','./assets/sprites/imgout/3/3-title.png');
        // this.load.image('m4-heart','./assets/sprites/imgout/4/4-heart.png');
        // this.load.image('m4-title','./assets/sprites/imgout/4/4-title.png');
        // this.load.image('missle','./assets/sprites/imgout/5/5-missile.png');
        // this.load.image('children','./assets/sprites/imgout/6/6-children.png');
        // this.load.image('children-bar','./assets/sprites/imgout/6/6-children-bar.png');
        // this.load.image('dinosaur','./assets/sprites/imgout/6/6-dinosaur.png');
        // this.load.image('bg','./assets/sprites/imgout/6/6-dinosaur-bar.png');
        // this.load.image('bg','./assets/sprites/imgout/6/6-fire.png');
        // this.load.image('bg','./assets/sprites/imgout/6/6-missile.png');
        // this.load.image('bg','./assets/sprites/imgout/6/6-time-bar.png');
        // this.load.image('bg','./assets/sprites/imgout/6/6-top.png');
        // this.load.image('bg','./assets/sprites/imgout/7/7-pop.png');
        // this.load.image('bg','./assets/sprites/imgout/7/restart.png');
        // this.load.image('bg','./assets/sprites/imgout/8/8-pop.png');
        // this.load.audio('bgmusic',['./assets/audio/bgmusic.mp3','./assets/audio/bgmusic.ogg']);

    }
    create(){
        this.add.sprite(0,0,'bg');
        // this.bgmusic = this.add.audio('bgmusic');
        // this.bgmusic.loop = true;
        // this.bgmusic.play();
        this.drawBoard =this.add.sprite(this.world.centerX,this.world.centerY,'draw-board');
        this.drawBoard.anchor.setTo(.5);

        this.m3Title = this.add.sprite(this.world.centerX,this.world.centerY*0.4,'m3-title');
        this.m3Title.anchor.setTo(.5);

        this.continueBtn = this.add.button(this.world.centerX,this.world.centerY*1.6,'continue-btn',this.startM4);
        this.continueBtn.anchor.setTo(.5);
   
        


    }
    startM4(){
        this.game.state.start('Menu4');
    }
}