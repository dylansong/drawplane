export default class Preload extends Phaser.State{
    preload(){
        this.load.image('m1-game-rule','./assets/sprites/imgout/1/1-game-rule.png');
        this.load.image('m1-start-btn','./assets/sprites/imgout/1/1-start-btn.png');
        this.load.image('m1-title','./assets/sprites/imgout/1/1-title.png');
        this.load.image('m2-title','./assets/sprites/imgout/2/2-title.png');
        this.load.image('continue-btn','./assets/sprites/imgout/2/continue-btn.png');
        this.load.image('draw-board','./assets/sprites/imgout/2/drawboard.png');
        this.load.image('m3-title','./assets/sprites/imgout/3/3-title.png');
        this.load.image('m4-heart','./assets/sprites/imgout/4/4-heart.png');
        this.load.image('m4-title','./assets/sprites/imgout/4/4-title.png');
        this.load.image('cannonball','./assets/sprites/imgout/5/5-missile.png');
        this.load.image('m5-title','./assets/sprites/imgout/5/5-title.png');
        this.load.image('children','./assets/sprites/imgout/6/6-children.png');
        this.load.image('childrenBar','./assets/sprites/imgout/6/6-children-bar.png');
        this.load.image('dinosaur','./assets/sprites/imgout/6/6-dinosaur.png');
        this.load.image('dinosaurBar','./assets/sprites/imgout/6/6-dinosaur-bar.png');
        this.load.image('fire','./assets/sprites/imgout/6/6-fire.png');
        this.load.image('missile','./assets/sprites/imgout/6/6-missile.png');
        this.load.image('timeBar','./assets/sprites/imgout/6/6-time-bar.png');
        this.load.image('top','./assets/sprites/imgout/6/6-top.png');
        this.load.image('successPop','./assets/sprites/imgout/7/7-pop.png');
        this.load.image('restart','./assets/sprites/imgout/7/restart.png');
        this.load.image('falsePop','./assets/sprites/imgout/8/8-pop.png');
        this.load.audio('bgmusic',['./assets/audio/bgmusic.mp3','./assets/audio/bgmusic.ogg']);
        this.load.audio('success',['./assets/audio/success.mp3']);
        this.load.audio('false',['./assets/audio/false.mp3']);
        this.load.audio('bonus',['./assets/audio/bonus.mp3']);

    }
    create(){
              this.add.sprite(0,0,'bg');
        this.state.start('Menu1');

    }
}