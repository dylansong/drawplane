export default class Preload extends Phaser.State{
    preload(){


    }
    create(){
        this.add.sprite(0,0,'bg');
        this.game.bgmusic = this.add.audio('bgmusic');
        this.game.bgmusic.loop = true;
        this.game.bgmusic.play();
        this.m1GameRule = this.add.sprite(this.world.centerX,this.world.centerY,'m1-game-rule');
        this.m1GameRule.anchor.setTo(.5);
        this.m1Title = this.add.sprite(this.world.centerX,this.world.centerY*0.5,'m1-title');
        this.m1Title.anchor.setTo(.5);
        this.m1StartBtn = this.add.button(this.world.centerX,this.world.centerY*1.48,'m1-start-btn',this.startM2);
        this.m1StartBtn.anchor.setTo(.5);

        this.add.tween(this.m1Title.scale).to({x:1.15,y:1.2},600,Phaser.Easing.Bounce.Out,true,0,-1,true);
        this.add.tween(this.m1Title).to({angle:'+1.3'},600,Phaser.Easing.Back.Out,true,0,-1,true);
        this.add.tween(this.m1Title).to({angle:'-1.3'},600,Phaser.Easing.Back.Out,true,0,-1,true);
   
        


    }
    startM2(){
        this.game.state.start('Menu2');
    }
}