export default class Boot extends Phaser.State{
    preload(){
        this.stage.backgroundColor = '#fff';
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.load.image('bg','./assets/sprites/imgout/1/bg.png');

    }
    create(){
        this.state.start('Preload');
    }
}