export default class Preload extends Phaser.State{
    preload(){


    }
    create(){
        this.add.sprite(0,0,'bg');
        // this.bgmusic = this.add.audio('bgmusic');
        // this.bgmusic.loop = true;
        // this.bgmusic.play();
        this.drawBoard =this.add.sprite(this.world.centerX,this.world.centerY*1.06,'draw-board');
        this.drawBoard.anchor.setTo(.5);

        this.m5Title = this.add.sprite(this.world.centerX,this.world.centerY*0.45,'m5-title');
        this.m5Title.anchor.setTo(.5);

        this.cannonball = this.add.sprite(this.world.centerX-150,this.world.centerY,'cannonball');
        this.cannonball.anchor.setTo(.5);

        this.add.tween(this.cannonball.scale).to({x:1.15,y:1.2},600,Phaser.Easing.Bounce.Out,true,0,-1,true);
        this.add.tween(this.cannonball).to({x:'+300'},600,Phaser.Easing.Back.Out,true,0,-1,true);
        

        this.continueBtn = this.add.button(this.world.centerX,this.world.centerY*1.625,'continue-btn',this.startGame);
        this.continueBtn.anchor.setTo(.5);
   
        


    }
    startGame(){
        this.game.state.start('PlayGame');
    }
}